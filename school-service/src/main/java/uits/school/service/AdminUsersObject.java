package uits.school.service;

import uits.school.domain.Users;

public class AdminUsersObject extends UsersObject {

    private String login;
    private String password;
    private boolean active;

    public AdminUsersObject() {
    }

    public AdminUsersObject(Users user) {
        super(user);
        this.login = user.getLogin();
        this.active = user.getActive();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
