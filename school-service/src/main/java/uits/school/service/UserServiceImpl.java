package uits.school.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import uits.school.domain.Users;
import uits.school.domain.UsersDao;

@Transactional
@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UsersDao usersDao;

    @Override
    public Users byId(Integer id) {
        return usersDao.byId(id);
    }

    @Override
    public Serializable save(Users object) {
        return usersDao.save(object);
    }

    @Override
    public void update(Users object) {
        usersDao.update(object);
    }

    @Override
    public void delete(Users object) {
        usersDao.remove(object);
    }

    @Override
    public List<Users> list() {
        return usersDao.list();
    }
    
    @Override
    public UsersObject getUserById(Integer id){
        //Users user = usersDao.byId(id);
        return new UsersObject(usersDao.byId(id));
    }

    @Override
    public AdminUsersObject getFullUserById(Integer id) {
        return new AdminUsersObject(usersDao.byId(id));
    }

    @Override
    public AdminUsersListFrameObject getUsersFrame(int offset, int limit) {
        int totalUsers = usersDao.getUsersCount();
        List<Users> users = usersDao.getUsers(offset, limit);
        return new AdminUsersListFrameObject(users, totalUsers);
    }
}
