
package uits.school.service;

import java.util.ArrayList;
import java.util.List;
import uits.school.domain.Media;
import uits.school.domain.Posts;
import uits.school.domain.Users;

public class UsersObject {
    private Integer id;
    private String firstname;
    private String lastname;
    private List<PostObject> posts;
    private List<MediaObgect> media;

    public UsersObject() {
    }

      public UsersObject(Users user) {
          this.id = user.getId();
          this.firstname = user.getFirstname();
          this.lastname = user.getLastname();
          this.posts = new ArrayList<>();
          for (Posts p : user.getPostsList()) {
              this.posts.add(new PostObject(p));
          }
          
          this.media = new ArrayList<>();
          for (Media p : user.getMediaList()) {
              this.media.add(new MediaObgect(p));
          }
    } 
      
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public List<PostObject> getPosts() {
        return posts;
    }

    public void setPosts(List<PostObject> posts) {
        this.posts = posts;
    }

    public List<MediaObgect> getMedia() {
        return media;
    }

    public void setMedia(List<MediaObgect> media) {
        this.media = media;
    }
    
    
    
}
