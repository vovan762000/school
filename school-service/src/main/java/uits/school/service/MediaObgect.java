
package uits.school.service;

import java.util.Date;
import uits.school.domain.Media;

public class MediaObgect {
    private Integer id;
    private String media;
    private Date dateCreated;
    private String mediaType;

    public MediaObgect() {
    }

     public MediaObgect(Media m) {
         this.id = m.getId();
         this.media = m.getMedia();
         this.dateCreated = m.getDateCreated();
         this.mediaType = m.getMediaType();
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }
    
    
}
