
package uits.school.service;

import java.util.ArrayList;
import java.util.List;
import uits.school.domain.Users;

public class AdminUsersListFrameObject {
    private int totalUsers;
    private List<AdminUsersObject> users;

    public AdminUsersListFrameObject() {
    }
    
    public AdminUsersListFrameObject(List<Users> users,int totalUsers) {
        this.totalUsers = totalUsers;
        this.users = new ArrayList<>();
        for (Users u : users) {
            this.users.add(new AdminUsersObject(u));
        }
    }

    public int getTotalUsers() {
        return totalUsers;
    }

    public void setTotalUsers(int totalUsers) {
        this.totalUsers = totalUsers;
    }

    public List<AdminUsersObject> getUsers() {
        return users;
    }

    public void setUsers(List<AdminUsersObject> users) {
        this.users = users;
    }
    
}
