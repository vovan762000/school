package uits.school.service;

import uits.school.domain.Users;

public interface UserService extends BaseService<Users, Integer>{
    public UsersObject getUserById(Integer id);
    public AdminUsersObject getFullUserById(Integer id);
    public AdminUsersListFrameObject getUsersFrame(int offset,int limit);
    public Users byId(Integer id);
}
