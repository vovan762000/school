
package uits.school.service;

import uits.school.domain.Posts;

public class PostObject {
    private Integer id;
    private String post;

    public PostObject() {
    }

     public PostObject(Posts p) {
       this.id = p.getId();
       this.post = p.getPost();
    }
     
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }
    
    
}
