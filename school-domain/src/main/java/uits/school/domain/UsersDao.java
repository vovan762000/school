package uits.school.domain;

import java.util.List;

public interface UsersDao extends BaseDao<Users, Integer> {
    int getUsersCount();
    List<Users> getUsers(int offset,int limit);
    public Users getByUsername(String login);
}
