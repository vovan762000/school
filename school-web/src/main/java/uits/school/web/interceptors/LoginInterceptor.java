package uits.school.web.interceptors;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if (request.getRequestURI().contains("/home")) {
            Cookie sid = getCookie(request);
            if (sid == null || !sid.getValue().equals(((HttpServletRequest) request).getSession().getId())) {
                response.sendRedirect("login");
            }
        }
    }

    private Cookie getCookie(ServletRequest request) {
        Cookie[] cookies = ((HttpServletRequest) request).getCookies();
        for (Cookie cooky : cookies) {
            if (cooky.getName().equals("sid")) {
                return cooky;
            }
        }
        return null;
    }

}
