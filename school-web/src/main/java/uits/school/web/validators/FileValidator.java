package uits.school.web.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import uits.school.web.objects.UploadedFile;

@Component
public class FileValidator implements Validator {

    private static final String IMAGE_PATTERN
            = "([^\\s]+(\\.(?i)(jpg|png))$)";

    @Override
    public void validate(Object uploadedFile, Errors errors) {

        UploadedFile file = (UploadedFile) uploadedFile;

        javaxt.io.Image image = new javaxt.io.Image(file.getFile().getOriginalFilename());
        System.out.println(file.getFile().getOriginalFilename());
        if (file.getFile().getSize() == 0) {
            errors.rejectValue("file", "uploadForm.selectFile", "Please select a file!");
        } else if (file.getFile().getSize() > 512000) {
            errors.rejectValue("file", "uploadForm.selectFile", "File too big!");
        }
//        else if (!isPicture(file.getFile().getName())) {
//            errors.rejectValue("file", "uploadForm.selectFile", "Not image!");
//        }

    }

    @Override
    public boolean supports(Class<?> clazz) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean isPicture(String testString) {
        Pattern p = Pattern.compile(IMAGE_PATTERN);
        Matcher m = p.matcher(testString);
        return m.matches();
    }
}
