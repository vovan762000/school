
package uits.school.web.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import uits.school.domain.Users;
import uits.school.domain.UsersDao;
import uits.school.service.AdminUsersObject;
import uits.school.service.UserService;

@Controller
public class ProfileController {
    @Autowired
    UserService userService;
    @Autowired
    private UsersDao usersDao;

    @ResponseBody
    @Transactional
    @RequestMapping(value = "profile")
    public ModelAndView getIndex() {
        ModelAndView mav = new ModelAndView("profile");
        return mav;
    }

    @ResponseBody
    @Transactional
    @RequestMapping(value = {"profile"}, method = RequestMethod.POST)
    public ModelAndView getEdit(@RequestParam Integer id) {
        AdminUsersObject user = userService.getFullUserById(id);
        return new ModelAndView("profile");
    }
}
