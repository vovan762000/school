package uits.school.web.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import uits.school.domain.Users;
import uits.school.service.AdminUsersObject;
import uits.school.service.UserService;
import uits.school.web.objects.UploadedFile;
import uits.school.web.validators.FileValidator;


@Controller
public class ImageController {

    @Autowired
    private FileValidator fileValidator;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "main", method = RequestMethod.GET)
    public ModelAndView getUpImage(@RequestParam Integer id) {
        AdminUsersObject user = userService.getFullUserById(id);
        ModelAndView mav = new ModelAndView("main","user",user);
        return mav;
    }

    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    public ModelAndView uploadFile(@ModelAttribute("uploadedFile") UploadedFile uploadedFile,
            @RequestParam Integer id,
            BindingResult result) {

        ModelAndView modelAndView = new ModelAndView();

        String fileName = null;

        MultipartFile file = uploadedFile.getFile();
        fileValidator.validate(uploadedFile, result);

        if (result.hasErrors()) {
            modelAndView.setViewName("main");
        } else {

            try {
                byte[] bytes = file.getBytes();

                fileName = file.getOriginalFilename();

                String rootPath = System.getProperty("catalina.home");
                File dir = new File(rootPath + File.separator + "tmpFiles");

                if (!dir.exists()) {
                    dir.mkdirs();
                }

                File loadFile = new File(dir.getAbsolutePath() + File.separator + fileName);

                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(loadFile));
                stream.write(bytes);
                stream.flush();
                stream.close();

                javaxt.io.Image image = new javaxt.io.Image(dir.getAbsolutePath() + File.separator + fileName);
                image.resize(120,120);
                byte[] newBytes = image.getByteArray();
                BufferedOutputStream stream1 = new BufferedOutputStream(new FileOutputStream(loadFile));
                stream1.write(newBytes);
                stream1.flush();
                stream1.close();
             
                Users user = userService.byId(id);
                user.setAva("files" + File.separator + file.getOriginalFilename());
                userService.update(user);

                RedirectView redirectView = new RedirectView("profile");
                redirectView.setStatusCode(HttpStatus.FOUND);
                modelAndView.setView(redirectView);
                modelAndView.addObject("user", user);

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        return modelAndView;
    }
}
