package uits.school.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {
    
    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login(){
        return "login";
    }
    
    @ResponseBody
    @RequestMapping(value = {"/login"}, method = RequestMethod.POST)
    public String submit(@RequestParam("username") String username,
            @RequestParam("password") String password,
            @RequestParam(name = "password1", required = false, defaultValue = "default") String password1){
        return username + " : " + password + " : " + password1;
    }
}
