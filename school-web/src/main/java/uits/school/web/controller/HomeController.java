package uits.school.web.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import uits.school.domain.Users;
import uits.school.domain.UsersDao;
import uits.school.service.AdminUsersObject;
import uits.school.service.UserService;

@Controller
public class HomeController {

    @Autowired
    UserService userService;
    @Autowired
    private UsersDao usersDao;

    @Transactional
    @RequestMapping(value = "home")
    public ModelAndView getIndex() {
        List<AdminUsersObject> users = new ArrayList<>();
        for (Users user : usersDao.getUsers(0, usersDao.getUsersCount())) {
            users.add(new AdminUsersObject(user));
        }
        ModelAndView mav = new ModelAndView("home", "users", users);
        return mav;
    }

    @Transactional
    @RequestMapping(value = {"edit"}, method = RequestMethod.POST)
    public ModelAndView getEdit(@RequestParam Integer id) {
        AdminUsersObject user = userService.getFullUserById(id);
        return new ModelAndView("edit", "user", user);
    }

}
