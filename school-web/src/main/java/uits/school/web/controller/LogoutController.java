
package uits.school.web.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LogoutController {
    @RequestMapping(value = {"logout"}, method = RequestMethod.GET)
    public ModelAndView submit(HttpServletResponse resp,
            HttpServletRequest req,
            SessionStatus sessionStatus) {
         sessionStatus.setComplete();
        resp.addCookie(new Cookie("sid", ""));
        return new ModelAndView("login");
    }
}
