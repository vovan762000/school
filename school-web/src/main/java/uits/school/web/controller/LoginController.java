package uits.school.web.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import uits.school.domain.Users;
import uits.school.domain.UsersDao;
import uits.school.service.AdminUsersObject;

@SessionAttributes("user")
@Controller
public class LoginController {

    @Autowired
    private UsersDao usersDao;

    @Transactional
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView main(HttpSession session) {
        return new ModelAndView("login");
    }

    @Transactional
    @RequestMapping(value = "/check-user", method = RequestMethod.POST)
    public ModelAndView checkUser(@RequestParam("login") String login,
            @RequestParam("password") String password,
            HttpServletResponse resp,
            HttpSession session) {
        Users user = usersDao.getByUsername(login);
        if (user != null && user.getPassword().equals(password)) {
            List<AdminUsersObject> users = new ArrayList<>();
            for (Users usr : usersDao.getUsers(0, usersDao.getUsersCount())) {
                users.add(new AdminUsersObject(usr));
            }
            resp.addCookie(new Cookie("sid", session.getId()));
            return new ModelAndView("home","user",user);
        }
        return new ModelAndView("login");
    }

}
