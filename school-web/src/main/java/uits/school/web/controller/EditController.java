package uits.school.web.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import uits.school.domain.Users;
import uits.school.domain.UsersDao;
import uits.school.service.AdminUsersObject;
import uits.school.service.UserService;

@Controller
public class EditController {

    @Autowired
    private UserService userService;
    @Autowired
    private UsersDao usersDao;


    @Transactional
    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public ModelAndView getIndex(@RequestParam Integer id) {
        AdminUsersObject user = userService.getFullUserById(id);
        ModelAndView mav = new ModelAndView("edit", "user", user);
        return mav;
    }

    @Transactional
    @RequestMapping(value = "home", method = RequestMethod.POST)
    public ModelAndView editUser(@RequestParam Integer id,
            @RequestParam("firstname") String firstname,
            @RequestParam("lastname") String lastname,
            SessionStatus sessionStatus) {

        Users user = userService.byId(id);
        user.setFirstname(firstname);
        user.setLastname(lastname);
        userService.update(user);
        List<AdminUsersObject> newUsers = new ArrayList<>();
        for (Users oldUser : usersDao.getUsers(0, usersDao.getUsersCount())) {
            newUsers.add(new AdminUsersObject(oldUser));
        }
        ModelAndView mav = new ModelAndView("home","users",newUsers);
        return mav;
    }

}
