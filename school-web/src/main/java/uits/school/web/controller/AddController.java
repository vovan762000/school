package uits.school.web.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import uits.school.domain.Media;
import uits.school.domain.Posts;
import uits.school.domain.Users;
import uits.school.domain.UsersDao;
import uits.school.service.AdminUsersObject;
import uits.school.service.UserService;

@Controller
public class AddController {

    @Autowired
    private UserService userService;
    @Autowired
    private UsersDao usersDao;

    @Transactional
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public ModelAndView getIndex() {
        ModelAndView mav = new ModelAndView("add", "newUser", new Users());
        return mav;
    }

    @Transactional
    @RequestMapping(value = {"add"}, method = RequestMethod.POST)
    public ModelAndView getEdit(@ModelAttribute Users newUser,
            @RequestParam("firstname") String firstname,
            @RequestParam("lastname") String lastname,
            @RequestParam("login") String login,
            @RequestParam("password") String password) {

        newUser.setFirstname(firstname);
        newUser.setLastname(lastname);
        newUser.setLogin(login);
        newUser.setPassword(password);
        newUser.setActive(Boolean.TRUE);
        newUser.setMediaList(new ArrayList<Media>());
        newUser.setPostsList(new ArrayList<Posts>());
        userService.save(newUser);
          List<AdminUsersObject> users = new ArrayList<>();
            for (Users usr : usersDao.getUsers(0, usersDao.getUsersCount())) {
                users.add(new AdminUsersObject(usr));
            }
        ModelAndView mav = new ModelAndView("home","users",users);
        return mav;
    }
}
