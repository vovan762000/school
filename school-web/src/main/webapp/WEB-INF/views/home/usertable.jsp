
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div id="page-wrapper">
    <div class="container-fluid">             
        <div class="row">
            <div class="col-lg-12">
                <h2>Users</h2>
                <div class="form-group" >
                    <input type="text" name="post" class="form-control"  placeholder="Searh" >
                </div>
                <form:form  method="POST" action="">
                    <td><input class="btn btn-success" type="submit" value="Searh" name="edit/"></td>
                    </form:form>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Username</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="users" items="${users}">
                                <tr>
                                    <td>${users.id}</td>
                                    <td>${users.login}</td>
                                    <td>${users.firstname}</td>
                                    <td>${users.lastname}</td>
                                    <form:form  method="POST" commandName="users" action="edit?id=${users.id}">
                                        <td><input class="btn btn-success" type="submit" value="edit" name="edit/"></td>
                                        </form:form>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <a href="add" class="btn btn-warning">Add user</a>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
