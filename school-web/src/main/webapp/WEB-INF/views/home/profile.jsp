<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../commons/header.jsp" %>
<%@include file="../commons/sidebar.jsp" %>
<head>
    <link href="static/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="static/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<div id="page-wrapper">

    <div class="container-fluid"> 
        <form action="" method="POST">
            <div class="form-group" >
                <label>Text Input with Placeholder</label>
                <input type="text" name="post" class="form-control"  placeholder="Enter text" >
            </div>
            <input class="btn btn-success" type="submit" value="Save" name="Save/">
        </form>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Add content<b class="caret"></b></a>
            <ul class="dropdown-menu alert-dropdown">
                <li>
                    <a href="#"><span class="label label-default">Video</span></a>
                </li>
                <li>
                    <a href="#"><span class="label label-primary">Audio</span></a>
                </li>
                <li>
                    <a href="main"><span class="label label-success">Image</span></a>
                </li>
            </ul>
        </li>
        <div class="row">
            <div class="col-lg-12">
                <h2>My Posts</h2>
                <div class="table-responsive">
                    <table class="table table-hover table-hover">
                        <tbody>                
                            <c:forEach var="posts" items="${posts}">
                                <tr>
                                    <c:if test="${posts.users.id == user.id}">
                                    <td>${posts.post} <br/> ${posts.dateCreated}</td> 
                                    </c:if>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <%@include file="../commons/footer.jsp" %>