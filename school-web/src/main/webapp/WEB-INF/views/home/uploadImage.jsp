<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="../commons/header.jsp" %>
<%@include file="../commons/sidebar.jsp" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <form:form action="uploadImage" method="POST" enctype="multipart/form-data">
            <div class="form-group" >
                <input type="file" name="file"/> 
            </div>
            <input class="btn btn-success" type="submit" value="Save" name="Save/">
        </form:form>
    </body>
</html>
<%@include file="../commons/footer.jsp" %>