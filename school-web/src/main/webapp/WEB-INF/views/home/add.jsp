
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
    <form:form  method="POST" commandName="user" action="add">
        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th colspan="2">Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                        
                                <tr>
                                    <td>Firstname</td>
                                    <td>
                                        <input class="form-control" type="text" name="firstname"/>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Lastname</td>
                                    <td>
                                        <input class="form-control" type="text" name="lastname"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Login</td>
                                    <td>
                                        <input class="form-control" type="text" name="login"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td>
                                        <input class="form-control" type="password" name="password"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input class="btn btn-success" type="submit" value="Add User" name="Save/"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
    </form:form>
</html>
