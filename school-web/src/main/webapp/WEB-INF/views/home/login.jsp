<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="<c:url value="/static/css/home.css" />" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>

<body>

	<form:form method="POST" modelAttribute="user" action="check-user" class="box login">

		<fieldset class="boxBody">

			<label>Login:</label>
			<input name="login" />

			<label>Password:</label>
			<input name="password"/>

		</fieldset>

		<footer>  
			<input type="submit" class="btnLogin" value="Login" tabindex="4"> 
		</footer>

	</form:form>


</body>
</html>