<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%--<%@include file="../commons/header.jsp" %>
<%@include file="../commons/sidebar.jsp" %>--%>

<div id="page-wrapper">
    <div class="container-fluid">             
        <div class="row">
            <div class="col-lg-12">
                <h2>Users</h2>
                <div class="table-responsive">
                    <form:form  method="POST" commandName="user" action="home?id=${user.id}">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th colspan="2">Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Username</td>
                                    <td>${user.login}</td>
                                </tr>
                                <tr>
                                    <td>Firstname</td>
                                    <td>
                                        <input class="form-control" type="text" name="firstname" value="${user.firstname}"/>

                                    </td>
                                </tr>
                                <tr>
                                    <td>Lastname</td>
                                    <td>
                                        <input class="form-control" type="text" name="lastname" value="${user.lastname}"/>
                                    </td>
                                </tr>

                            <td>
                                
                                
                            </td>
                            <tr><td><input class="btn btn-success" type="submit" value="Save" name="Save/"/></td></tr>
                            <tr><td><a href="main?id=${user.id}" class="btn btn-success">Upload Avatar</a></td></tr>                     
                            <td><a href="home" class="btn btn-warning">Back</a></td>
                            </tbody>
                        </table>
                    </form:form>
                </div>
            </div>
        </div>
    </div>

</div>
<%--<%@include file="../commons/footer.jsp" %>--%>
